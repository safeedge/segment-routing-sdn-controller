#!/usr/bin/env python3

# Copyright (C) 2019 Simon Redman sredman@cs.utah.edu
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
# implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# This file is kind of the analogue to sr_flow_mgmt.py, but for dealing with the nlsdn-running routers


import ipaddress
from collections import namedtuple
import json
import logging
import random
import requests
import socket

import nlsdn.server.sdn as nlsdn

from typing import List, Dict

from northbound_match import Match
from northbound_actions import Actions

LOG = logging.getLogger('ryu.app.nlsdn_controller')
LOG.setLevel(logging.DEBUG)

NumberedResource = namedtuple("NumberedResource", ['id', 'resource',])

class nlsdnController():

    NLSDN_TEMPLATE = "https://{host}:{port}/service/nlsdn/v1/{endpoint}"
    NL_TEMPLATE = "https://{host}:{port}/service/nl/v1/{endpoint}"

    # NLSDN endpoints
    MATCH_ENDPOINT="match"
    ACTION_ENDPOINT="action"

    # Raw NL endpoints
    ROUTE_ENDPOINT="routes"

    def __init__(self, nlsdn_id_map, username, password, management_port=56789, verify_certs=True):
        """

        :param nlsdn_id_map: Mapping of nlsdn IDs to management IP addresses
        :param username: Login username for nlsdn clients
        :param password: Login password for nlsdn clients
        :param management_port: Listening port of the nlsdn devices
        :param verify_certs: Check cert of nlsdn controller REST API for validity
        """
        self.id_map: Dict[int, str] = nlsdn_id_map
        self.management_port = management_port
        self.verify_certs = verify_certs
        self.auth = requests.auth.HTTPBasicAuth(username, password)

    @staticmethod
    def _get_route_attr(route, attr_name: str):
        """
        Get the value of the requested attribute of the route
        """
        maybe_value = [attr[1] for attr in route['attrs'] if attr[0] == attr_name]
        if len(maybe_value) == 0:
            return None
        elif len(maybe_value) == 1:
            return maybe_value.pop()
        else:
            raise Exception(f"Too many values found in {route} with attribute name {attr_name}")

    def get_routes(self, nlsdn_id: int, filter=None):
        """
        Get all the routes of the requested router
        """

        if not nlsdn_id in self.id_map:
            raise NoSuchIDException("nlsdn node with ID {id} does not exist".format(id=id))

        management_ip = ipaddress.ip_address(self.id_map[nlsdn_id])

        url = self.NL_TEMPLATE.format(host=management_ip, port=self.management_port, endpoint=self.ROUTE_ENDPOINT)

        routes_response = requests.get(url, auth=self.auth, verify=self.verify_certs)
        routes_response.raise_for_status()

        routes = json.loads(routes_response.content)
        return routes

    def get_ipv6_routes(self, nlsdn_id: int):
        """
        Return only the routes which involve ipv6 addresses
        """
        routes = self.get_routes(nlsdn_id)

        ipv6_routes = []
        for route in routes:
            family = route['family']
            # Assume anything which isn't IPv4 is IPv6 (Not future proof. Will happily come out of retirement post ~2050
            # for one last hurrah to think of something more robust
            if family != socket.AF_INET:
                ipv6_routes.append(route)
        return ipv6_routes

    def get_matches(self, nlsdn_id: int) -> List[NumberedResource]:
        """
        Get all matches on a particular node
        """
        return self._get_nlsdn(nlsdn_id, endpoint=self.MATCH_ENDPOINT, resource="match")

    def get_actions(self, nlsdn_id: int) -> List[NumberedResource]:
        """
        Get all matches on a particular node
        """
        return self._get_nlsdn(nlsdn_id, endpoint=self.ACTION_ENDPOINT, resource="action")

    def _get_nlsdn(self, nlsdn_id: int, endpoint: str, resource: str) -> List[NumberedResource]:
        management_ip = ipaddress.ip_address(self.id_map[nlsdn_id])
        port = self.management_port
        match_url = self.NLSDN_TEMPLATE.format(host=management_ip, port=port, endpoint=endpoint)
        matches_response = requests.get(match_url, auth=self.auth, verify=self.verify_certs)
        matches_response.raise_for_status()

        matches_json = json.loads(matches_response.content)

        matches: List[NumberedResource] = []
        for _id, resource_body in matches_json.items():
            type = resource_body['type']
            tkwargs = resource_body
            del tkwargs['type']
            matches.append(NumberedResource(_id, nlsdn.Engine.factory(resource, type, **tkwargs)))
        return matches

    def insert_single_flow(self, nlsdn_id: int, match: Match, action: Actions):
        """
        Insert a single flow rule: based on the given matches, assign the given SRv6 midpoints

        Throws NoSuchIDException if the nlsdn_id does not correspond to an actual node

        :param nlsdn_id: nlsdn node to push the rules
        :param match:
        :param action:
        :return:
        """

        if not nlsdn_id in self.id_map:
            raise NoSuchIDException("nlsdn node with ID {id} does not exist".format(id=nlsdn_id))

        management_ip = ipaddress.ip_address(self.id_map[nlsdn_id])
        port = self.management_port

        nlsdn_match = match.to_nlsdn()
        nlsdn_action = action.to_nlsdn()

        if nlsdn_match.src is None or nlsdn_match.dst is None:
            raise ValueError("Must specify both source IP and dest IP of match")

        # Check for family type mismatch
        for family, addr_type in [(socket.AF_INET, ipaddress.IPv4Address), (socket.AF_INET6, ipaddress.IPv6Address), ]:
            if match.match_fields['family'] is family:
                for ip in [match.match_fields[field] for field in ['ipv6_src', 'ipv6_dst']]:
                    if not isinstance(ip, addr_type):
                        # Maybe this should correct the error instead of exploding, but meh
                        raise AddressFamilyError(f"IP Address {ip} is not of the specified IP family (IPv4 vs. IPv6?)")

        routes = self.get_ipv6_routes(nlsdn_id)

        # Find the route which leads towards the destination of this action
        segs = action.get_actions_fields()['srv6_dst']
        if len(segs) == 0:
            # No segments? Okay, just use the default destination of the flow
            # (Could probably bail at this point and let default kernel routing handle this case but whatever)
            dest_ip = match.get_match_fields()['ipv6_dst']
        else:
            # We are routing towards the first segment
            dest_ip = segs[0]

        route = None
        for potential_route in routes:
            dst_len: int = potential_route['dst_len']
            table: int = int(self._get_route_attr(potential_route, "RTA_TABLE"))
            if (table >= 4096):
                # This is not a valid table for a route because it is one we pushed!)
                continue
            dest: str = self._get_route_attr(potential_route, "RTA_DST")
            dest_network = ipaddress.ip_network(str(dest) + "/" + str(dst_len), strict=False)
            if dest_ip in dest_network:
                route = potential_route

        if route is None:
            raise RouteNotFoundError(f"Could not find route for {str(dest_ip)}")

        LOG.debug(f"Trying to get output interface for flow matching {nlsdn_match} on dpid {nlsdn_id}")
        output_iface = self._get_route_attr(route, "RTA_OIF")
        if output_iface is None:
            # Multipath routing is more different
            multipath = self._get_route_attr(route, "RTA_MULTIPATH")
            selected_path = random.choice(multipath)
            output_iface = selected_path['oif'] # Wouldn't it be too easy if the format of this OIF were the same as the non-multipath?

        match_url = self.NLSDN_TEMPLATE.format(host=management_ip, port=port, endpoint=self.MATCH_ENDPOINT)
        action_url = self.NLSDN_TEMPLATE.format(host=management_ip, port=port, endpoint=self.ACTION_ENDPOINT)

        # NB: for seg6 inline mode, the kernel looks up routes only in the
        # table that it started in, so we can't use the default route, because
        # segments will be looked up in the same table.  So just do this for
        # both encap and inline mode.  This is obviously not a generic TE
        # assumption, but in our case, all flows are ipv6 and we always have
        # src/dst.
        if match.get_match_fields()['ipv6_dst']:
            nlsdn_action.dst = match.get_match_fields()['ipv6_dst'].compressed
            nlsdn_action.dst_len = 128

        nlsdn_action.oif = output_iface

        json_action = nlsdn_action.to_json()

        action_response = requests.post(action_url, json=json_action, auth=self.auth, verify=self.verify_certs)
        action_response.raise_for_status()

        action_id = json.loads(action_response.content)

        nlsdn_match.action_id = action_id
        json_match = nlsdn_match.to_json()

        match_response = requests.post(match_url, json=json_match, auth=self.auth, verify=self.verify_certs)
        if 400 <= match_response.status_code < 500:
            error_msg = json.loads(match_response.content)['error']
            match_response.reason = match_response.reason + " - " + error_msg
        match_response.raise_for_status()

        pass

    def delete_flows(self, nlsdn_id: int, delete_filter: Match=None):
        """
        Delete all flows which match the specified match of the specified router

        :param nlsdn_id:
        :param delete_filter: Optional match to match for deletion. If none is specified, delete all rules
        :return:
        """
        delete_filter = delete_filter.to_nlsdn() if delete_filter else None

        management_ip = ipaddress.ip_address(self.id_map[nlsdn_id])
        port = self.management_port

        if not nlsdn_id in self.id_map:
            raise NoSuchIDException("nlsdn node with ID {id} does not exist".format(id=nlsdn_id))

        matches = self.get_matches(nlsdn_id)
        actions = self.get_actions(nlsdn_id)

        deleting_matches: List[int] = []
        deleting_actions: List[int] = []
        for id, match in matches:
            if delete_filter is None or match in delete_filter:
                deleting_matches.append(id)

        for action_id, action in actions:
            for match_id in deleting_matches:
                if match_id in action.matches:
                    del action.matches[match_id]
            if len(action.matches) == 0:
                deleting_actions.append(action_id)

        deleting_matches.sort()
        deleting_actions.sort()
        url_template = self.NLSDN_TEMPLATE.format(host=management_ip, port=port, endpoint="{endpoint}/{id}")

        if len(deleting_matches) == 0 and delete_filter is not None:
            # If we were given a filter but nothing was actually matched, that's an error
            raise NoSuchMatchError()

        for endpoint, resource_ids in [(self.MATCH_ENDPOINT, deleting_matches), (self.ACTION_ENDPOINT, deleting_actions)]:
            for id in resource_ids:
                url = url_template.format(endpoint=endpoint, id=id)
                response = requests.delete(url, auth=self.auth, verify=self.verify_certs)
                response.raise_for_status()

        pass


class NoSuchMatchError(Exception):
    pass

class RouteNotFoundError(Exception):
    pass

class NoSuchIDException(Exception):
    pass

class AddressFamilyError(Exception):
    pass