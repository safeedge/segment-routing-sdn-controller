# Copyright (C) 2017 Binh Nguyen binh@cs.utah.edu.
# Copyright (C) 2018 Simon Redman sredman@cs.utah.edu
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
# implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import ipaddress
import logging
from nlsdn.server.sdn import Seg6Action
import socket

LOG = logging.getLogger('ryu.app.Actions')
LOG.setLevel(logging.INFO)

from typing import List

class Actions(object):

    def to_nlsdn(action) -> Seg6Action:
        enacap = {"segs": list(map(str, action.actions_fields['srv6_dst']))}
        return Seg6Action(
            encap=enacap,
            family=action.actions_fields['family'],
        )
  
    def get_actions_fields(self):
        return self.actions_fields

    @staticmethod
    def _parse_field(field_name: str, field_value):
        # TODO: rest of fields
        if field_name == 'srv6_dst':
            srv6_dsts: List[ipaddress.IPv6Address] = list(map(ipaddress.ip_address, field_value))
            return srv6_dsts

        raise UnknownActionException(field_name)

    def __init__(self, **actions):

        self.actions_fields = {
            # All supported actions fields. eg, curl -d "actions="ipv6_dst=::01,mod_dl_src="AA:BB:CC:DD:EE:FF""
            "srv6_dst": None,
            "mod_dl_dst": None,
            "output": None,
            "family": socket.AF_INET6,
        }

        for action in actions:
            self.actions_fields[action] = Actions._parse_field(action, actions[action])

    def __str__(self):
        return str(self.actions_fields)

    def print_me(self):
        LOG.info("Actions_fields -> value")
        for key in self.actions_fields:
            LOG.info("%s -> %s" % (key, self.actions_fields[key]))


class UnknownActionException(Exception):
    pass
