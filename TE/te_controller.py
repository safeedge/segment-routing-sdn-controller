# Copyright (C) 2017 Binh Nguyen binh@cs.utah.edu.
# Copyright (C) 2018 Simon Redman sredman@cs.utah.edu
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
# implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from ryu.app.wsgi import ControllerBase
import ipaddress
import logging
from netdiff import NetJsonParser
import networkx
from threading import Lock
from webob import Response

LOG = logging.getLogger('ryu.app.Te_controller')
LOG.setLevel(logging.DEBUG)


class Te_controller(ControllerBase):
    # This graph stores the best-known current topology
    graph = NetJsonParser(data={"type": "NetworkGraph",
                                "protocol": "static",
                                "version": None,
                                "metric": None,
                                "nodes": [],
                                "links": []})
    graph_lock = Lock()
    static_topology = None  # This must be populated before any request for the graph data is made

    def __init__(self, req, link, data, **config):
        super(Te_controller, self).__init__(req, link, data, **config)
        self.data = data

    def netjson_import(self, req, **kwargs):
        with Te_controller.graph_lock:
            incoming_json = req.body.decode(req.charset)
            importing_graph = NetJsonParser(incoming_json)

            # ASSUMPTION WARNING
            # I have conveniently used each node's IP address as its OSPF ID. That means, for me, it is
            # safe and useful to convert the incoming labels back to dotted-quad form
            mapping = {}
            for id in importing_graph.graph.nodes:
                dotted_quad = ipaddress.IPv4Address(id)
                mapping[id] = str(dotted_quad)
                del dotted_quad
                del id
            importing_graph.graph = networkx.relabel_nodes(importing_graph.graph, mapping)
            del mapping

            # Netdiff library is supposed to be able to detect differences in graphs, but it is crashing
            # for me for reasons I can't figure out. Do the job by hand.

            known_nodes = Te_controller.graph.graph.nodes
            known_edges = Te_controller.graph.graph.edges
            importing_nodes = importing_graph.graph.nodes
            importing_edges = importing_graph.graph.edges

            # Add any new edges or nodes
            for node in importing_nodes:
                Te_controller.graph.graph.add_node(node)
            for edge in importing_edges:
                Te_controller.graph.graph.add_edge(*edge)

            # Check the properties of all incoming edges and nodes and record any new ones
            for (known_edges_or_nodes, importing_edges_or_nodes) in ((known_nodes, importing_nodes), (known_edges, importing_edges)):
                for edge_or_node in importing_edges_or_nodes:
                    known_data = known_edges_or_nodes[edge_or_node]
                    for key, value in importing_edges_or_nodes[edge_or_node].items():
                        if isinstance(value, dict):
                            # Extend same-key dictionaries with new data
                            if not key in known_data:
                                known_data[key] = {}
                            known_data[key].update(value)
                        elif isinstance(value, list):
                            # Extend same-key lists with new data (block duplicates)
                            if not key in known_data:
                                known_data[key] = []
                            # Ensure no duplicates. This is very slow but handles unhashable things
                            for thing in value:
                                if not thing in known_data[key]:
                                    known_data[key].append(thing)
                        else:
                            # The only other valid value types should be str or int
                            if value is not None:
                                known_data[key] = value

            # Ensure all edges have a "weight" key to avoid crash in netdiff
            for edge in Te_controller.graph.graph.edges:
                if 'weight' not in Te_controller.graph.graph.edges[edge]:
                    Te_controller.graph.graph.edges[edge]['weight'] = None

            # Check for any known links which are missing in the incoming data
            for edge in known_edges:
                if edge in importing_edges:
                    # Nothing to do, this link is still alive
                    continue

                (u, v) = edge[:2]
                if u == v:
                    # ignore self-loops
                    continue

                information_valid = False
                for node in edge:
                    # If the link is missing but the reporter knows about ones of the nodes on the link
                    # the information is probably good
                    if node in importing_nodes:
                        information_valid = True
                if not information_valid:
                    # If the reporter doesn't know about either of the nodes in the questionable link,
                    # there is (probably) a network partition and we need to rely on other reporters
                    # (on the other side of that partition) to handle the situation
                    continue

                known_edges[edge]['down'] = True
        dead_edges = [edge for edge in known_edges if 'down' in known_edges[edge]]

        for edge in dead_edges:
            Te_controller.graph.graph.remove_edge(*edge)

        return Response(status=200, body="Yay")

    def force_upload_netjson(self, req, **_kwargs):
        """
        Tranple the known graph with an uploaded graph. Mostly useful for debugging
        """
        with Te_controller.graph_lock:
            incoming_json = req.body.decode(req.charset)
            importing_graph = NetJsonParser(incoming_json)
            Te_controller.graph = importing_graph

    #REST API - Return the topology graph, handle OPTIONS request in preflight request
    def handle_get_topology_OPTIONS(self, req, **_kwargs):
        headers = {
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Methods': 'GET, POST',
        'Access-Control-Allow-Headers': 'Origin, Content-Type',
                    'Content-Type':'application/json'}
        return Response(content_type='application/json', headers=headers)

    def get_topology_netjson(self, req, **_kwargs):
        headers = {
            'Access-Control-Allow-Origin': '*', # Anybody may request this resource
            'Access-Control-Allow-Methods': 'GET',
            'Access-Control-Allow-Headers': 'Origin, Content-Type',
        }

        # Copy the static graph by converting it to JSON then back. This stops us from having to
        # hold the graph_lock for the whole time we are working
        with Te_controller.graph_lock:
            data = Te_controller.graph.json(dict=True)
        combined_graph = NetJsonParser(data)

        # Insert all the OVS bridges in their rightful place
        # This is trickier than it sounds: Since they are invisible in the live topology, we need
        # to use the static topology to find the neighbor they should have, then break the links on those
        # neighbors, insert the OVS node, and insert new links

        # (Live) OVS nodes are any which have registered with us. When they are registered, we install their OpenFlow
        # datapath ID under the key 'dpid'
        ovs_nodes = [node for node in Te_controller.static_topology.graph.nodes if 'dpid' in Te_controller.static_topology.graph.nodes[node]]
        for ovs in ovs_nodes:
            combined_graph.graph.add_node(ovs, **Te_controller.static_topology.graph.nodes[ovs])
            static_neighbors = Te_controller.static_topology.graph.adj[ovs]
            edges_to_delete = set()
            for neighbor in static_neighbors:
                # Figure out which original links should be replaced by links passing through the OVS
                # Such links would be nodes which are neighbors of the OVS in the static graph, but which are
                # neighbors of each other in the live graph
                for other_neighbor in static_neighbors:
                    if neighbor == other_neighbor:
                        continue
                    edge = (neighbor, other_neighbor)
                    if edge in combined_graph.graph.edges:
                        # Add a link between this neighbor and the OVS
                        original_edge_params = combined_graph.graph.edges[edge]
                        combined_graph.graph.add_edge(neighbor, ovs, **original_edge_params)
                        # Prepare the original edge for deletion
                        edges_to_delete.add(edge)
            for edge in edges_to_delete:
                # This check is theoretically not necessary, but since we are dealing with an undirected
                # graph we will try to delete the same edge twice every time
                if edge in combined_graph.graph.edges:
                    combined_graph.graph.remove_edge(*edge)

        # Give all nodes in the live topology labels based on matching the management IP of nodes
        # in the static topology to the router ID in the live topology
        for node in combined_graph.graph.nodes:
            if not node in Te_controller.static_topology.graph.nodes:
                # Something odd has happened because this node was detected in the live topology
                # but was not provided to us as part of the static topology. Leave it without a label.
                continue
            combined_graph.graph.nodes[node]['label'] = Te_controller.static_topology.graph.nodes[node]['label']
            combined_graph.graph.nodes[node]['interfaces'] = Te_controller.static_topology.graph.nodes[node]['interfaces']
            # TODO: Would probably be useful to copy ALL properties from the static graph which are not already in the combined graph
            pass

        for edge in combined_graph.graph.edges:
            if not edge in Te_controller.static_topology.graph.edges:
                # Something odd has happened because this edge was detected in the live topology
                # but was not provided to us as part of the static topology. Leave it alone.
                continue
            for copied_key in ['bandwidth', ]:
                static_edge =  Te_controller.static_topology.graph.edges[edge]
                if copied_key in static_edge:
                    combined_graph.graph.edges[edge][copied_key] = static_edge[copied_key]
            # TODO: Would probably be useful to copy ALL properties from the static graph which are not already in the combined graph
            pass

        # Prepare JSON for returning
        data = combined_graph.json(dict=True)
        # Convert the IP address prefixes to strings for presentation by a GUI
        for nodes_or_edges in [data['nodes'], data['links']]:
            for node_or_edge in nodes_or_edges:
                if not ('properties' in node_or_edge and 'prefixes' in node_or_edge['properties']):
                    continue
                serialized_prefixes = node_or_edge['properties']['prefixes']
                string_prefixes = []
                for prefix in serialized_prefixes:
                    string = "{prefix}/{len}".format(prefix=prefix['prefix'],
                                                     len=prefix['length'])
                    string_prefixes.append(string)
                node_or_edge['properties']['prefixes'] = string_prefixes

        return Response(status=200,
                        content_type='application/json',
                        json_body=data,
                        headers=headers,)
