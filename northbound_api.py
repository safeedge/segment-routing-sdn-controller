# Copyright (C) 2017 Binh Nguyen binh@cs.utah.edu.
# Copyright (C) 2018 Simon Redman sredman@cs.utah.edu
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
# implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import logging
from webob import Response
from northbound_match import Match as Match
from northbound_actions import Actions as Actions
from sr_flows_mgmt import SR_flows_mgmt as SR_flows_mgmt
import nlsdn_controller
from nlsdn_controller import nlsdnController
from ospf_monitor import *
import json
from typing import List

from ryu.app.wsgi import ControllerBase


LOG = logging.getLogger('ryu.app.North_api')
LOG.setLevel(logging.INFO)
HEADERS = {
                        'Access-Control-Allow-Origin': '*',
                        'Access-Control-Allow-Methods': 'GET, POST',
                        'Access-Control-Allow-Headers': 'Origin, Content-Type',
                        'Content-Type':'application/json'}

class North_api(ControllerBase):
    def __init__(self, req, link, data, **config):
        super(North_api, self).__init__(req, link, data, **config)
        self.nlsdn_controller: nlsdnController = data['nlsdn-controller']

    def delete_single_flow(self, req, **_kwargs):
        """
        Delete one or more flows from the specified router which match the passed match

        Params:
          - dpid: ID of router to delete from
          - match: Match rules as defined by the Match type

        Example usage:
            curl -H "Content-Type: application/json" --data '{ "dpid":2,
                            "match": {
                                "ipv6_dst": "fd00:10:10:12:1::fed2",
                                "ipv6_src": "fd00:10:10:5::/64"
                            }
                }' http://localhost:8080/flow_mgmt/delete
            Will command the SDN router with ID 2 to delete the match on all traffic destined for fd00:10:10:12:1::fed
            from the subnet fd00:10:10:5::/64
        """
        body = json.loads(s=req.body.decode('utf-8'))
        SR = SR_flows_mgmt()

        for required_key in ['dpid', 'match']:
            if not required_key in body:
                error = "Invalid POST; missing key {key}".format(key=required_key)
                LOG.info(error)
                return Response(status=422, headers=HEADERS, body=error)

        match = Match(**body['match'])
        dpid = int(body['dpid'])
        priority = 0
        if 'priority' in body:
            priority = int(body['priority'])

        # Check whether we are doing nlsdn or OpenFlow
        nlsdn_mode = False
        openflow_mode = False
        if dpid in SR.dpid_to_datapath:
            openflow_mode = True
            LOG.warning("OpenFlow Mode Detected. This will almost certainly not work because the author is not maintaining OpenFlow in favour of nlsdn")

        if dpid in self.nlsdn_controller.id_map:
            nlsdn_mode = True

        if not (nlsdn_mode or openflow_mode):
            raise ValueError("DPID {dpid} matched neither openflow nor nlsdn".format(dpid=dpid))
        if nlsdn_mode and openflow_mode:
            raise ValueError("Found a dpid match for both openflow and nlsdn")

        LOG.info("RECEIVED NB API: delete_single_flow: (dpid, match) = (%s, %s)" % (dpid, match) )

        if nlsdn_mode:
            try:
                self.nlsdn_controller.delete_flows(dpid, match)
                return Response(status=200, headers=HEADERS)
            except nlsdn_controller.NoSuchMatchError:
                return Response(
                    status=404,
                    headers=HEADERS,
                    body="No matches of node with ID {dpid} matched filter".format(dpid=dpid)
                )

        if openflow_mode:
            if SR.delete_single_flow(dpid, priority, match):
                LOG.info("Deleted a flow.")
                return Response(status=200, headers=HEADERS)
        return Response(status=500, headers=HEADERS)

    def delete_all_flows(self, req, **_kwargs):
        """
        Delete all flows of the reqested routers

        Params:
          - dpid: List of IDs which are matched to either OpenFlow or nlsdn nodes

        Example Usage:
            curl -H "Content-Type: application/json" --data '{ "dpids": [3] }'
                http://localhost:8080/flow_mgmt/delete_all_flows
            Will command the SDN router with ID 3 to drop its table of matches and actions
        """
        body = json.loads(s=req.body.decode('utf-8'))

        for required_key in ['dpids']:
            if not required_key in body:
                error = "Invalid POST; missing key {key}".format(key=required_key)
                LOG.info(error)
                return Response(status=422, headers=HEADERS, body=error)
        SR = SR_flows_mgmt()

        dpids: List[int] = list(map(lambda x: int(x), body['dpids']))

        for dpid in dpids:
            # Check whether we are doing nlsdn or OpenFlow
            nlsdn_mode = False
            openflow_mode = False
            if dpid in SR.dpid_to_datapath:
                openflow_mode = True
                LOG.warning("OpenFlow Mode Detected. This will almost certainly not work because the author is not maintaining OpenFlow in favour of nlsdn")

            if dpid in self.nlsdn_controller.id_map:
                nlsdn_mode = True

            if not (nlsdn_mode or openflow_mode):
                raise ValueError("DPID {dpid} matched neither openflow nor nlsdn".format(dpid=dpid))
            if nlsdn_mode and openflow_mode:
                raise ValueError("Found a dpid match for both openflow and nlsdn")

            LOG.debug("RECEIVED NB API: delete_all_flows: (dpid) = (%s)" % (dpid) )

            if nlsdn_mode:
                self.nlsdn_controller.delete_flows(dpid)
            if openflow_mode:
                if SR.delete_all_flows(dpid):
                    LOG.info("Deleted all flows in switch %s." % dpid)
        return Response(status=200, headers=HEADERS)

    def insert_single_flow(self, req, **_kwargs):
        """
        Insert a single flow

        This method was originally designed for the single-purpose of OpenFlow dpid inserts, but now handles
        both nlsdn and OpenFlow. Thus, dpid is used for both cases, and we just check for a node in either
        which is assigned a matching ID

        Params:
          - dpid: ID of router to push a rule
          - match: Match rules as defined by the Match type
          - action: Action rules as defined by the Actions type
          - priority (optional): OpenFlow rule priority value. Not used by nlsdn (at the moment)

        Example usage:
            curl -H "Content-Type: application/json" --data '{ "dpid":2,
                            "match": {
                                "ipv6_dst": "fd00:10:10:12:1::fed2",
                                "ipv6_src": "fd00:10:10:5::/64"
                            },
                            "actions": {
                                "srv6_dst": ["fd00:10:10:3:1::efdd","fd00:10:10:4:1::4f4f"]
                            }
                }' http://localhost:8080/flow_mgmt/insert
            Will command the SDN router with ID 2 to match all traffic destined for fd00:10:10:12:1::fed
            from the subnet fd00:10:10:5::/64 to go via fd00:10:10:3:1::efdd then fd00:10:10:4:1::4f4f
        """
        body = json.loads(s=req.body.decode('utf-8'))

        for required_key in ['dpid', 'match', 'actions']:
            if not required_key in body:
                error = "Invalid POST; missing key {key}".format(key=required_key)
                LOG.info(error)
                return Response(status=422, headers=HEADERS, body=error)

        actions = Actions(**body['actions'])
        match = Match(**body['match'])
        dpid = int(body['dpid'])
        SR = SR_flows_mgmt()
        priority = 0
        if 'priority' in body:
            priority = int(body['priority'])

        # Check whether we are doing nlsdn or OpenFlow
        nlsdn_mode = False
        openflow_mode = False
        if dpid in SR.dpid_to_datapath:
            openflow_mode = True
            LOG.warning("OpenFlow Mode Detected. This will almost certainly not work because the author is not maintaining OpenFlow in favour of nlsdn")

        if dpid in self.nlsdn_controller.id_map:
            nlsdn_mode = True

        if not (nlsdn_mode or openflow_mode):
            raise ValueError("DPID {dpid} matched neither openflow nor nlsdn".format(dpid=dpid))
        if nlsdn_mode and openflow_mode:
            raise ValueError("Found a dpid match for both openflow and nlsdn")

        LOG.info("RECEIVED NB_API: insert_single_flow: (dpid, match, actions) = (%s,%s,%s)" % (dpid, match, actions))

        if nlsdn_mode:
            self.nlsdn_controller.insert_single_flow(dpid, match, actions)

        if openflow_mode:
            if not actions or not match:
                LOG.error("Actions or match fields are empty: actions = %s, match = %s" % (actions, match))
                return Response(status = 500, headers=HEADERS)
            if not SR.insert_single_flow(dpid, priority, match, actions):
                LOG.info("Inserted a flow.")
                return Response(status=200, headers=HEADERS)
            else:
                LOG.error("Can't insert a flow!")
                return Response(status=500, headers=HEADERS)

    def handle_http_options(self, req, **_kwargs):
                return Response(content_type='application/json', headers=HEADERS)

    def get_nlsdn_ids(self, req):
        """
        Return a dictionary mapping management IPs to ids useful for calling nlsdn endpoints

        Example usage: curl http://localhost:8080/nlsdn-mapping
        """
        headers = {
            'Access-Control-Allow-Origin': '*', # Anybody may request this resource
            'Access-Control-Allow-Methods': 'GET',
            'Access-Control-Allow-Headers': 'Origin, Content-Type',
        }
        id_mapping = self.nlsdn_controller.id_map

        # The mapping stored by nlsdn_controller is id -> IP. Swap it
        ip_mapping = { ip: nid for nid, ip in id_mapping.items() }

        return Response(status=200,
                        content_type='application/json',
                        json=ip_mapping,
                        headers=headers,
                        )

    def get_routes_of_node(self, req):
        """
        Return the result of asking for /service/nl/v1/routes
        (Assumes an nlsdn ID)
        :param req:
        """
        body = json.loads(s=req.body.decode('utf-8'))

        for required_key in ['dpid']:
            if not required_key in body:
                error = "Invalid POST; missing key {key}".format(key=required_key)
                LOG.info(error)
                return Response(status=422, headers=HEADERS, body=error)

        dpid: int = int(body['dpid'])

        routes = self.nlsdn_controller.get_routes(dpid)

        return Response(status=200,
                        content_type='application/json',
                        json=routes,
                        headers=HEADERS,
                        )
