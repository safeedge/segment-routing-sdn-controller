# Copyright (C) 2017 Binh Nguyen binh@cs.utah.edu.
# Copyright (C) 2018 Simon Redman sredman@cs.utah.edu
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
# implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import ipaddress
import logging
import nlsdn.server.sdn
import socket


LOG = logging.getLogger('ryu.app.Match')
LOG.setLevel(logging.INFO)

class Match:

    def to_nlsdn(match) -> nlsdn.server.sdn.Match:
        ipv6_src = match.match_fields['ipv6_src']
        ipv6_dst = match.match_fields['ipv6_dst']

        if ipv6_src:
            nlsdn_src = str(ipv6_src.ip)
            nlsdn_src_len = ipv6_src.network.prefixlen
        else:
            nlsdn_src = None
            nlsdn_src_len = None

        if ipv6_dst:
            nlsdn_dst = str(ipv6_dst.ip)
            nlsdn_dst_len = ipv6_dst.network.prefixlen
        else:
            nlsdn_dst = None
            nlsdn_dst_len = None

        sport_range = None
        sport = match.match_fields['sport']
        if sport is not None:
            # Have to convert to range, even for only one value, because that is how nlsdn works
            sport_range = [sport, sport]

        dport_range = None
        dport = match.match_fields['dport']
        if dport is not None:
            # Have to convert to range, even for only one value, because that is how nlsdn works
            dport_range = [dport, dport]

        #return nlsdn.server.sdn.RuleMatch(
        return nlsdn.server.sdn.NftablesRuleMatch(
            priority=match.match_fields['priority'],
            src=nlsdn_src,
            src_len=nlsdn_src_len,
            dst=nlsdn_dst,
            dst_len=nlsdn_dst_len,
            dscp=0,new_dscp=0x20,
            #sport_range=sport_range,
            #dport_range=dport_range,
            #ipproto=match.match_fields['ipproto'],
            family=match.match_fields['family'],
        )

    def get_match_fields(self):
        return self.match_fields

    @staticmethod
    def _parse_field(field_name: str, field_value):
        if field_name == 'in_port':
            return int(field_value)
        if field_name in ['sport', 'dport']:
            return int(field_value)
        if field_name == 'ipproto':
            return int(field_value)
        if field_name == 'eth_type':
            return hex(field_value)
        # TODO: rest of fields
        if field_name == 'ipv6_src' or field_name == 'ipv6_dst':
            return ipaddress.ip_interface(field_value)

        raise UnknownMatchException(field_name)


    def __init__(self, **kwagrs):
        self.match_fields = {  # all supported match fields. eg, curl -d "match="in_port=1,out_port=2,nw_src=::01""
            "priority": 0,
            "in_port": None,
            "eth_type": None,
            "out_port": None,
            "ipv6_src": None,
            "ipv6_dst": None,
            "sport": None,
            "dport": None,
            "ipproto": None,
            "dl_src": None,
            "dl_dst": None,
            "family": socket.AF_INET6, # AF_INET6 for IPv6 rule or AF_INET for IPv4 rule
        }

        for key in kwagrs:
            self.match_fields[key] = Match._parse_field(key, kwagrs[key])

    def __str__(self):
        return str(self.match_fields)

    def print_me(self):
        LOG.info("Match_fields -> value")
        for key in self.match_fields:
            LOG.info("%s -> %s" % (key, self.match_fields[key]))

class UnknownMatchException(Exception):
    pass